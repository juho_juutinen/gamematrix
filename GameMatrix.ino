/**
 * Copyright (c) 2015 Juho Juutinen.
 * 
 * This file is part of GameMatrix.
 * 
 * GameMatrix is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later 
 * version.
 * 
 * GameMatrix is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with GameMatrix. If not, see <http://www.gnu.org/licenses/>.
 *
 * @file:    GameMatrix.ino
 * @author:  Juho Juutinen <juho.juutinen@iki.fi>
 * @created: 04/18/2015
 */

/*==== LIBRARY INCLUSIONS ===================================================*/

// Wireless includes
#include <JeeLib.h>

// LED Matrix includes
#include <Adafruit_GFX.h>
#include <Adafruit_NeoMatrix.h>
#include <Adafruit_NeoPixel.h>

/*==== CONSTANT/MACRO DEFINITIONS ===========================================*/

// Macro constants
#define MATRIX_WIDTH        10  // Width of display matrix (pixels)
#define MATRIX_HEIGHT       20  // Height of display matrix (pixels)
#define MATRIX_PIXELS       200 // Total amount of pixels
#define MATRIX_BRIGHTNESS   255 // Default brightness (0 - 255)
#define MATRIX_VERTICAL     0   // Vertical orientation of display matrix
#define MATRIX_HORIZONTAL   1   // Horizontal orientation of display matrix
#define MATRIX_PIN          6   // Pin ID for matrix I/O

#define SERIAL_RATE         9600   // Serial port data transmission rate (bps)
#define REFRESH_RATE        100    // Time waited between frames (milliseconds)
#define CONTROLLER_BUTTONS  8      // Total amount of accessible buttons
#define FPS_60              17     // delay to achieve roughly 60 fps (milliseconds)
#define FPS_30              33     // delay to achieve roughly 30 fps (milliseconds)

// Enumerable constants
enum KEY {A, B, SELECT, START, UP, DOWN, LEFT, RIGHT};
enum DIRECTION {NORTH, SOUTH, EAST, WEST};

// Common color constants
const uint16_t
COLOR_RED    = 0xF800, COLOR_GREEN  = 0x07E0, COLOR_BLUE   = 0x001F,
COLOR_YELLOW = 0xFFE0, COLOR_BLACK  = 0x0000, COLOR_WHITE  = 0xFFFF,
COLOR_AQUA   = 0x07FF, COLOR_VIOLET = 0xF81F, COLOR_OLIVE  = 0x8400,
COLOR_PINK   = 0xFE19, COLOR_ORANGE = 0xFD20, COLOR_TEAL   = 0x0410;

const uint16_t TEXT_COLOR = 0x07FF;

// NES controller I/O pins
const byte NES_DATA = 9;    // Data connection pin
const byte NES_LATCH = 11;  // Latch/strobe pin
const byte NES_CLOCK = 12;  // Clock pin

/*==== VARIABLE DEFINITIONS =================================================*/

// Function pointers to program states
void (*nextState)() = 0;
void (*currentState)() = 0;
void (*previousState)() = 0;
bool exitingState = false;

// Struct definitions
typedef struct {
    int x;
    int y;
} point;

// Display matrix definition
typedef Adafruit_NeoMatrix NM;
uint8_t layout = NEO_MATRIX_TOP + NEO_MATRIX_LEFT +
                 NEO_MATRIX_ROWS + NEO_MATRIX_ZIGZAG;
uint8_t pixeltype = NEO_GRB + NEO_KHZ800;
NM matrix = NM(MATRIX_WIDTH, MATRIX_HEIGHT, MATRIX_PIN, layout, pixeltype);

// Arrays for button state data
bool keyState [CONTROLLER_BUTTONS];
bool keyPState [CONTROLLER_BUTTONS];

/*==== MAIN SETUP ===========================================================*/

void setup() {
    Serial.begin(SERIAL_RATE);
    gamepadSetup();
    matrix.begin();
    matrix.setTextWrap(false);
    matrix.setBrightness(MATRIX_BRIGHTNESS);
    matrix.setTextColor(TEXT_COLOR);
    matrix.setRotation(MATRIX_HORIZONTAL);
    currentState = &stateMenu;
}

/*==== MAIN LOOP ============================================================*/

void loop() {

    // Run current program state function
    exitingState = false;
    currentState();

    // Switch to next state
    if (nextState != 0) {
        previousState = currentState;
        currentState = nextState;
    }
}

/*==== CONTROLLER FUNCTIONS =================================================*/

// Update controls and handle state exit
void updateControls() {
    gamepadUpdate();
    
    // Exit current game state and return to menu with start + select
    if (key(START) && key(SELECT)) {
        nextState = &stateMenu;
        exitingState = true;
    }
}

// Set up NES controller I/O
void gamepadSetup() {
    pinMode(NES_LATCH, OUTPUT);
    pinMode(NES_CLOCK, OUTPUT);
    pinMode(NES_DATA, INPUT);
}

// Retrieve current NES controller state
void gamepadUpdate() {
    digitalWrite(NES_LATCH, HIGH);   // Send a clock pulse to the latch
    digitalWrite(NES_LATCH, LOW);    // Save button status within the 4021 chip

    // Transmit data from NES controller
    for(int x = 0; x <= CONTROLLER_BUTTONS - 1; x++) {
        keyPState[x] = keyState[x];
        bitWrite(keyState[x], 0, !digitalRead(NES_DATA));
        digitalWrite(NES_CLOCK, HIGH);
        digitalWrite(NES_CLOCK, LOW);
    }
}

// Return state of controller button [b] (enum KEY)
bool key(int b) { return keyState[b]; }
bool keyUp(int b) { return !keyState[b]; }
bool keyDown(int b) { return (keyState[b] && !keyPState[b]); }

/*==== HELPER FUNCTIONS =====================================================*/

// Linear interpolation between points [p1], [p2] in interval [t]
float lerp(float p1, float p2, float t) {
  return (1 - t) * p1 + t * p2;
}

// Keep referenced int [v] between range [min] - [max]
void limit(int &v, int min, int max) {
    if (v < min) v = min;
    if (v > max) v = max;
}

// Add [c] to referenced int [v] but keep it between range [min] - [max]
void limit(int &v, int c, int min, int max) {
    v += c;
    if (v < min) v = min;
    if (v > max) v = max;
}

// Keep referenced int [v] between range [min] - [max], wrap if exceeds
void wrap(int &v, int min, int max) {
    if (v < min) v = max;
    if (v > max) v = min;
}

// Convert byte [step] to 24-bit color in RGBR gradient
uint32_t gradient24(byte step) {
    step = 255 - step;
    if (step < 85) {
        return color24(255 - step * 3, 0, step * 3);
    } else if (step < 170) {
        step -= 85;
        return color24(0, step * 3, 255 - step * 3);
    } else {
        step -= 170;
        return color24(step * 3, 255 - step * 3, 0);
    }
}

// Convert byte [step] to 16-bit color in RGBR gradient
uint16_t gradient(byte step) {
    step = 255 - step;
    if (step < 85) {
        return color(255 - step * 3, 0, step * 3);
    } else if (step < 170) {
        step -= 85;
        return color(0, step * 3, 255 - step * 3);
    } else {
        step -= 170;
        return color(step * 3, 255 - step * 3, 0);
    }
}

// Convert color channels [r][g][b] into a packed 24-bit RGB color
uint32_t color24(uint8_t r, uint8_t g, uint8_t b) {
  return ((uint32_t)r << 16) | ((uint32_t)g <<  8) | b;
}

// Convert color channels [r][g][b] into a packed 16-bit RGB color
uint16_t color(uint8_t r, uint8_t g, uint8_t b) {
  return ((uint16_t)(r & 0xF8) << 8) |
         ((uint16_t)(g & 0xFC) << 3) |
                    (b         >> 3);
}

// Return a random 24-bit RGB color
uint16_t rngColor24() {
    return color24(random(0, 255), random(0, 255), random(0, 255));
}

// Return a random 16-bit RGB color
uint16_t rngColor() {
    return color(random(0, 255), random(0, 255), random(0, 255));
}

/*==== VISUAL EFFECTS =================================================*/

// Render current pixels and wait for [t] milliseconds
bool gfxUpdate(int t) {
    matrix.show();
    delay(t);
    return gfxInterrupt();
}

// Skip currently playing effect if START key is pressed
bool gfxInterrupt() {
    gamepadUpdate();
    if (key(START)) {
        return true;
    }
    return false;
}

// Fill the screen one pixel at a time
void gfxColorfill(uint32_t color, int duration) {
    int wait = duration / MATRIX_PIXELS / 2;
    for (int i = 0; i < MATRIX_PIXELS; i++) {
        matrix.setPixelColor(i, color);
        if (gfxUpdate(wait)) return;
    }
    for (int i = MATRIX_PIXELS; i >= 0; i--) {
        matrix.setPixelColor(i, 0);
        if (gfxUpdate(wait)) return;
    }
}

// Fill the screen one pixel at a time in rainbow color
void gfxColorfill(int duration) {
    int step = 0;
    int wait = (duration / MATRIX_PIXELS) * 2;
    for (int i = 0; i < MATRIX_PIXELS; i++) {
        step++;
        wrap(step, 0, 255);
        matrix.setPixelColor(i, gradient24(step));
        if (gfxUpdate(wait)) return;
    }
    for (int i = MATRIX_PIXELS; i >= 0; i--) {
        step++;
        wrap(step, 0, 255);
        matrix.setPixelColor(i, gradient24(step));
        if (gfxUpdate(wait)) return;
    }
}

// Fill the screen in a spiraly motion
void gfxLoop(uint16_t color, int duration) {
    int width = matrix.width();
    int height = matrix.height();
    int wait = duration / MATRIX_PIXELS;
    int offset = 0;
    
    while (offset < height / 2) {
        
        for (int x = offset; x < width - offset; x++) {
            matrix.drawPixel(x, offset, color);
            if (gfxUpdate(wait)) return;
        }
        
        for (int y = offset; y < height - offset; y++) {
            matrix.drawPixel(width - offset - 1, y, color);
            if (gfxUpdate(wait)) return;
        }
        
        for (int x = width - offset; x > offset; x--) {
            matrix.drawPixel(x, height - offset - 1, color);
            if (gfxUpdate(wait)) return;
        }
        
        for (int y = height - offset; y > offset; y--) {
            matrix.drawPixel(offset, y, color);
            if (gfxUpdate(wait)) return;
        }
        
        offset++;
    }
}

// Fill the screen in a spiraly motion with random color
void gfxLoopback(int duration) {
    int width = matrix.width();
    int height = matrix.height();
    int wait = duration / MATRIX_PIXELS;
    int offset = 0;
    uint16_t color = rngColor();

    while (offset < height / 2) {
        for (int x = offset; x < width - offset; x++) {
            matrix.drawPixel(x, offset, color);
            if (gfxUpdate(wait)) return;
        }

        for (int y = offset; y < height - offset; y++) {
            matrix.drawPixel(width - offset - 1, y, color);
            if (gfxUpdate(wait)) return;
        }
        
        for (int x = width - offset; x > offset; x--) {
            matrix.drawPixel(x, height - offset - 1, color);
            if (gfxUpdate(wait)) return;
        }
        
        for (int y = height - offset; y > offset; y--) {
            matrix.drawPixel(offset, y, color);
            if (gfxUpdate(wait)) return;
        }
        
        offset++;
    }

    color = rngColor();

    while (offset != 0) {
        for (int y = height - offset; y > offset; y--) {
            matrix.drawPixel(offset, y, color);
            if (gfxUpdate(wait)) return;
        }

        for (int x = width - offset; x > offset; x--) {
            matrix.drawPixel(x, height - offset - 1, color);
            if (gfxUpdate(wait)) return;
        }

        for (int y = offset; y < height - offset; y++) {
            matrix.drawPixel(width - offset - 1, y, color);
            if (gfxUpdate(wait)) return;
        }

        for (int x = offset; x < width - offset; x++) {
            matrix.drawPixel(x, offset, color);
            if (gfxUpdate(wait)) return;
        }

        offset--;
    }
}

// Fill the screen with horizontal lines
void gfxHLinefill(uint16_t color, int duration) {
    int width = matrix.width();
    int height = matrix.height();
    int wait = duration / width;

    for (int x = 0; x < width; x++) {
        matrix.fillScreen(COLOR_BLACK);
        for (int y = 0; y < height; y++) {
            if (y % 2) {
                matrix.drawLine(0, y, x, y, color);
            } else {
                matrix.drawLine(width, y, width - x - 1, y, color);
            }
        }
        if (gfxUpdate(wait)) return;
    }
}

// Fill the screen with randomly colored horizontal lines
void gfxHLinefill(int duration) {
    int width = matrix.width();
    int height = matrix.height();
    int wait = duration / width;
    uint16_t color = rngColor();

    for (int x = 0; x < width; x++) {
        matrix.fillScreen(COLOR_BLACK);
        for (int y = 0; y < height; y++) {
            if (y % 2) {
                matrix.drawLine(0, y, x, y, color);
            } else {
                matrix.drawLine(width, y, width - x - 1, y, color);
            }
        }
        if (gfxUpdate(wait)) return;
    }
}

// Fill the screen with vertical lines
void gfxVLinefill(uint16_t color, int duration) {
    int width = matrix.width();
    int height = matrix.height();
    int wait = duration / height;

    for (int y = 0; y < height; y++) {
        matrix.fillScreen(COLOR_BLACK);
        for (int x = 0; x < width; x++) {
            if (x % 2) {
                matrix.drawLine(x, 0, x, y, color);
            } else {
                matrix.drawLine(x, height, x, height - y - 1, color);
            }
        }
        if (gfxUpdate(wait)) return;
    }
}

// Fill the screen with randomly colored vertical lines
void gfxVLinefill(int duration) {
    int width = matrix.width();
    int height = matrix.height();
    int wait = duration / height;
    uint16_t color = rngColor();

    for (int y = 0; y < height; y++) {
        matrix.fillScreen(COLOR_BLACK);
        for (int x = 0; x < width; x++) {
            if (x % 2) {
                matrix.drawLine(x, 0, x, y, color);
            } else {
                matrix.drawLine(x, height, x, height - y - 1, color);
            }
        }
        if (gfxUpdate(wait)) return;
    }
}

// Fill the screen with stars
void gfxStars(uint16_t color, int duration) {
    int width = matrix.width();
    int height = matrix.height();
    int wait = duration / 100;
    int x = 5;
    int y = 5;

    for (int i = 0; i < 100; i++) {
        matrix.fillScreen(COLOR_BLACK);
        matrix.drawPixel(x, y, color);
        if (i % 2) {
            matrix.drawPixel(x + 1, y, color);
            matrix.drawPixel(x, y + 1, color);
            matrix.drawPixel(x - 1, y, color);
            matrix.drawPixel(x, y - 1, color);
        } else {
            matrix.drawPixel(x + 1, y + 1, color);
            matrix.drawPixel(x - 1, y + 1, color);
            matrix.drawPixel(x - 1, y - 1, color);
            matrix.drawPixel(x + 1, y - 1, color);
        }
        if (gfxUpdate(wait)) return;
    }
}

// Fill the screen with rainbow scale
void gfxRainbow(int duration) {
    int wait = duration / 256;
    uint16_t i, j;

    for (j = 0; j < 256; j++) {
        for (i = 0; i< MATRIX_PIXELS; i++) {
            matrix.setPixelColor(i, gradient24((i + j) & 255));
        }
        if (gfxUpdate(wait)) return;
    }
}

// Theatre-style crawling lights
void gfxTheater(uint32_t color, int duration) {
    int wait = duration / 30;

    for (int j = 0; j < 10; j++) {
        for (int q = 0; q < 3; q++) {
            for (int i = 0; i < MATRIX_PIXELS; i = i + 3) {
                matrix.setPixelColor(i+q, color); 
            }

            if (gfxUpdate(wait)) return;

            for (int i=0; i < MATRIX_PIXELS; i = i + 3) {
                matrix.setPixelColor( i+ q, 0); 
            }
        }
    }
}

// Theatre-style crawling lights with rainbow effect
void gfxTheaterRainbow(int duration) {
    int wait = duration / 256;

    for (int j = 0; j < 256; j++) {
        for (int q = 0; q < 3; q++) {
            for (int i = 0; i < MATRIX_PIXELS; i = i + 3) {
                matrix.setPixelColor(i + q, gradient24((i + j) % 255));
            }

            if (gfxUpdate(wait)) return;

            for (int i=0; i < MATRIX_PIXELS; i = i + 3) {
                matrix.setPixelColor(i + q, 0);
            }
        }
    }
}

// Fill the screen in a zigzag motion
void gfxCrossfill(uint16_t color, int duration) {
    int wait = duration / 104;
    int width = matrix.width();
    int height = matrix.height();
    int mid = height / 2;

    for (int y = 0; y <= 2; y += 2) {
        for (int x = 0; x < width; x++) {
            matrix.fillScreen(COLOR_BLACK);
            matrix.drawPixel(x, y, color);
            matrix.drawPixel(width - x, height - y - 1, color);
            if (gfxUpdate(wait)) return;
        }

        for (int x = width; x > 0; x--) {
            matrix.fillScreen(COLOR_BLACK);
            matrix.drawPixel(x, y + 1, color);
            matrix.drawPixel(width - x, height - y - 2, color);
            if (gfxUpdate(wait)) return;
        }
    }

    for (int x = 0; x < width; x++) {
        matrix.fillScreen(COLOR_BLACK);
        matrix.drawLine(0, mid - 1, x, mid - 1, color);
        matrix.drawLine(width - x, mid, width, mid, color);
        if (gfxUpdate(wait)) return;
    }

    for (int y = 4; y >= 0; y--) {
        matrix.fillScreen(COLOR_BLACK);
        matrix.drawLine(0, y, width, y, color);
        matrix.drawLine(0, height - y, width, height - y, color);
        if (gfxUpdate(wait)) return;
    }
}

