/**
 * Copyright (c) 2015 Juho Juutinen.
 * 
 * This file is part of GameMatrix.
 * 
 * GameMatrix is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later 
 * version.
 * 
 * GameMatrix is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with GameMatrix. If not, see <http://www.gnu.org/licenses/>.
 *
 * @file:    stateInputCheck.ino
 * @author:  Juho Juutinen <juho.juutinen@iki.fi>
 * @created: 04/21/2015
 */

/*==== STATE: INPUT CHECK ===================================================*/

void stateInputCheck() {

    // Transition effect
    gfxColorfill(COLOR_BLUE, 2000);

    // State loop
    while (!exitingState) {
        updateControls();
        matrix.fillScreen(COLOR_BLUE);
        matrix.setCursor(0, 2);
        
        // Draw pixels when controller buttons are pressed or released
        if (key(A)) {
            matrix.drawPixel(0, 0, COLOR_RED);
            matrix.print(F("A"));
            if (keyDown(A)) {
                matrix.drawPixel(0, 1, COLOR_GREEN);
            }
        }
        
        if (key(B)) {
            matrix.drawPixel(1, 0, COLOR_RED);
            matrix.print(F("B"));
            if (keyDown(B)) {
                matrix.drawPixel(1, 1, COLOR_GREEN);
            }
        }

        if (key(SELECT)) {
            matrix.drawPixel(2, 0, COLOR_RED);
            matrix.print(F("Select"));
            if (keyDown(SELECT)) { 
                matrix.drawPixel(2, 1, COLOR_GREEN);
            }
        }
        
        if (key(START)) {
            matrix.drawPixel(3, 0, COLOR_RED);
            matrix.print(F("Start"));
            if (keyDown(START)) { 
                matrix.drawPixel(3, 1, COLOR_GREEN);
            }
        }
        
        if (key(UP)) {
            matrix.drawPixel(4, 0, COLOR_RED);
            matrix.print(F("Up"));
            if (keyDown(UP)) {
                matrix.drawPixel(4, 1, COLOR_GREEN);
            }
        }
         
        if (key(DOWN)) {
            matrix.drawPixel(5, 0, COLOR_RED);
            matrix.print(F("Down"));
            if (keyDown(DOWN)) {
                matrix.drawPixel(5, 1, COLOR_GREEN);
            }
        }
        if (key(LEFT)) {
            matrix.drawPixel(6, 0, COLOR_RED);
            matrix.print(F("Left"));
            if (keyDown(LEFT)) {
                matrix.drawPixel(6, 1, COLOR_GREEN);
            }
        }
        
        if (key(RIGHT)) {
            matrix.drawPixel(7, 0, COLOR_RED);
            matrix.print(F("Right"));
            if (keyDown(RIGHT)) {
                matrix.drawPixel(7, 1, COLOR_GREEN);
            }
        }

        // Draw pixels
        matrix.show();
        delay(REFRESH_RATE);
    }
    return;
}