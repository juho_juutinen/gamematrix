/**
 * Copyright (c) 2015 Juho Juutinen.
 * 
 * This file is part of GameMatrix.
 * 
 * GameMatrix is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later 
 * version.
 * 
 * GameMatrix is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with GameMatrix. If not, see <http://www.gnu.org/licenses/>.
 *
 * @file:    stateSnake.ino
 * @author:  Juho Juutinen <juho.juutinen@iki.fi>
 * @created: 04/21/2015
 */

/*==== STATE: SNAKE GAME ====================================================*/

void stateSnake() {

    // Constants
    const int MaximumFood = 20;
    const int MaximumSize = MATRIX_PIXELS;
    const int DefaultSpeed = 400;
    const int MaximumSpeed = 50;
    const int SpeedStep = 50;
    const int FoodIntervalMin = 4;
    const int FoodIntervalMax = 14;

    // Game colors
    const uint16_t BodyColor = 0x07E0;
    const uint16_t HeadColor = 0x8400;
    const uint16_t FoodColor = 0xFFE0;

    // Local variables
    int foodTimer = 0;
    int currentSize = 2;
    int currentSpeed = DefaultSpeed;
    int currentDirection = EAST;
    bool extendSnake = false;
    bool gameOver = false;
    point currentTarget;

    // Arrays for snake parts and food
    point snake[MaximumSize + 1];
    point food[MaximumFood];

    // Reset food position (food with x of -1 is considered unused)
    for (int t = 0; t < MaximumFood; t++) {
        food[t].x = -1;
    }

    // Snake starting composition
    snake[0].x = 5; snake[0].y = 5;
    snake[1].x = 6; snake[1].y = 5;
    snake[2].x = 7; snake[2].y = 5;

    // Transition effect
    gfxColorfill(color24(0, 255, 0), 2000);

    // State loop
    while (!exitingState) {
        updateControls();
        matrix.fillScreen(COLOR_BLACK);

        // Periodically spawn some food
        if (foodTimer <= 0) {
            for (int t = 0; t < MaximumFood; t++) {
                if (food[t].x == -1) {
                    food[t].x = random(0, matrix.width() - 1);
                    food[t].y = random(0, matrix.height() - 1);
                    t = MaximumFood;
                }
            }
            foodTimer = random(FoodIntervalMin, FoodIntervalMax);
        } else {
            foodTimer--;
        }

        // Change direction of the snake, allow only 90 degree turns
        if (key(LEFT) && currentDirection != EAST) {
            currentDirection = WEST;
        } else if (key(RIGHT) && currentDirection != WEST) {
            currentDirection = EAST;
        } else if (key(UP) && currentDirection != SOUTH) {
            currentDirection = NORTH;
        } else if (key(DOWN) && currentDirection != NORTH) {
            currentDirection = SOUTH;
        }

        // Increase game speed with SELECT key
        if (keyDown(SELECT)) {
            limit(currentSpeed, -SpeedStep, MaximumSpeed, DefaultSpeed);
        }

        // Target position to move to
        currentTarget = snake[currentSize];

        // Attempt to move towards target direction
        if (currentDirection == NORTH) { currentTarget.y--; }
        if (currentDirection == SOUTH) { currentTarget.y++; }
        if (currentDirection == WEST) { currentTarget.x--; }
        if (currentDirection == EAST) { currentTarget.x++; }
          
        // Check if snake collided with edge of the arena
        if (currentTarget.y < 0 || currentTarget.y > matrix.height() - 1 ||
            currentTarget.x < 0 || currentTarget.x > matrix.width() - 1) {
            gameOver = true;
        }

        // Check if head hit another part of the snake
        for (int t = 0; t < currentSize; t++) {
            if (snake[t].x == currentTarget.x &&
                snake[t].y == currentTarget.y) {
                gameOver = true;
            }
        }

        // Check if head hit a food container
        for (int t = 0; t < MaximumFood; t++) {
            if (food[t].x == currentTarget.x &&
                food[t].y == currentTarget.y) {
                extendSnake = true;
                food[t].x = -1;
                foodTimer = 0;
            }
        }

        // Snake did not collide with anything; moving along
        if (!gameOver) {
            // Extend snake if food was eaten
            if (extendSnake && currentSize < MaximumSize) {
                currentSize++;
                extendSnake = false;
            } else {
                for (int t = 0; t < currentSize; t++) {
                    snake[t] = snake[t + 1];
                }
            }
            snake[currentSize] = currentTarget;

            // Draw food containers
            for (int t = 0; t < MaximumFood; t++) {
                if (food[t].x != -1) {
                    matrix.drawPixel(food[t].x, food[t].y, FoodColor);
                }
            }

            // Draw snake body
            for (int t = 0; t < currentSize; t++) {
                matrix.drawPixel(snake[t].x, snake[t].y, BodyColor);
            }
            
            // Draw snake head
            matrix.drawPixel(snake[currentSize].x, snake[currentSize].y, HeadColor);
            matrix.show();
            delay(currentSpeed);

        // Snake hit a wall or part of itself; the game is over
        } else {
            // Wait until player presses A key
            while (!keyDown(A)) {
                updateControls();
                for (int t = 0; t <= currentSize; t++) {
                    matrix.drawPixel(snake[t].x, snake[t].y, color(200, 20, 20));
                }
                matrix.show();
                delay(REFRESH_RATE);
            }

            // Reset game state
            currentSize = 2;
            foodTimer = 0;
            currentSpeed = DefaultSpeed;
            currentDirection = EAST;
            extendSnake = false;
            gameOver = false;
            for (int t = 0; t < MaximumFood; t++) {
                food[t].x = -1;
            }
            snake[0].x = 5; snake[0].y = 5;
            snake[1].x = 6; snake[1].y = 5;
            snake[2].x = 7; snake[2].y = 5;
            gfxColorfill(color24(0, 255, 0), 4000);
        }
    }
    return;
}