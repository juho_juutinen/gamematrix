/**
 * Copyright (c) 2015 Juho Juutinen.
 * 
 * This file is part of GameMatrix.
 * 
 * GameMatrix is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later 
 * version.
 * 
 * GameMatrix is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with GameMatrix. If not, see <http://www.gnu.org/licenses/>.
 *
 * @file:    stateLightshow.ino
 * @author:  Juho Juutinen <juho.juutinen@iki.fi>
 * @created: 04/21/2015
 */

/*==== STATE: LIGHTSHOW =====================================================*/

void stateLightshow() {

    // Constants
    const int sfxModes = 13;
    const int MinSpeed = 1000;
    const int MaxSpeed = 20000;
    const int SpeedStep = 1000;

    // Local variables
    int currentSFX = 0;
    int currentSpeed = MinSpeed;
    uint8_t colorR = 255;
    uint8_t colorG = 255;
    uint8_t colorB = 255;

    // State loop
    while (!exitingState) {
        matrix.fillScreen(COLOR_BLACK);
        updateControls();

        // Change selected effect with LEFT and RIGHT keys
        if (keyDown(LEFT)) { currentSFX--; }
        if (keyDown(RIGHT)) { currentSFX++; }
        limit(currentSFX, 0, sfxModes);

        // Change playback speed with UP and DOWN keys
        if (key(UP)) { currentSpeed -= SpeedStep; }
        if (key(DOWN)) { currentSpeed += SpeedStep; }
        limit(currentSpeed, MinSpeed, MaxSpeed);

        // Create random color with B key
        if (keyDown(B)) {
            colorR = random(0, 255);
            colorG = random(0, 255);
            colorB = random(0, 255);
        }

        // Start selected lightshow with A key
        if (keyDown(A)) {
            runLightshow(currentSFX, currentSpeed, colorR, colorG, colorB);
        }

        // Draw lines representing sfx and speed controls
        matrix.drawLine(0, 1, currentSFX, 1, COLOR_WHITE);
        matrix.drawPixel(0, 1, COLOR_BLUE);
        matrix.drawLine(0, 3, currentSpeed / SpeedStep, 3, COLOR_WHITE);
        matrix.drawPixel(1, 5, color(colorR, colorG, colorB));

        // Draw pixels
        matrix.show();
        delay(REFRESH_RATE);
    }
    return;
}

// Run special effect [sfx] for [d] milliseconds using color [r, g, b]
// if [sfx] equals zero, random special effects are shown instead
void runLightshow(int sfx, int d, uint8_t r, uint8_t g, uint8_t b) {

    // Enable random mode
    bool randomSFX = (sfx == 0);

    // Generate 16 and 24-bit colors
    uint16_t c = color(r, g, b);
    uint32_t c24 = color24(r, g, b);

    // Run selected effect until START key is pressed
    while (!key(START)) {
        matrix.fillScreen(COLOR_BLACK);
        if (randomSFX) {
            sfx = random(1, 13);
            c = rngColor();
            c24 = rngColor24();
        }
        switch (sfx) {
            case 1: gfxCrossfill(c, d);       break;
            case 2: gfxColorfill(c24, d);     break;
            case 3: gfxColorfill(d);          break;
            case 4: gfxHLinefill(c, d);       break;
            case 5: gfxHLinefill(d);          break;
            case 6: gfxVLinefill(c, d);       break;
            case 7: gfxVLinefill(d);          break;
            case 8: gfxLoop(c, d);            break;
            case 9: gfxLoopback(d);           break;
            case 10: gfxStars(c, d);          break;
            case 11: gfxTheater(c24, d);      break;
            case 12: gfxRainbow(d);           break;
            case 13: gfxTheaterRainbow(d);    break;
        }
    }
}

