/**
 * Copyright (c) 2015 Juho Juutinen.
 * 
 * This file is part of GameMatrix.
 * 
 * GameMatrix is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later 
 * version.
 * 
 * GameMatrix is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with GameMatrix. If not, see <http://www.gnu.org/licenses/>.
 *
 * @file:    stateMenu.ino
 * @author:  Juho Juutinen <juho.juutinen@iki.fi>
 * @created: 04/21/2015
 */

/*==== STATE: MAIN MENU =====================================================*/

void stateMenu() {

    // Constants
    const int Modes = 5; // Total amount of available modes
    const int LSize = 5; // Width of one letter (pixels)

    // Local variables
    int selectedMode = 0;
    int textPosition = 0;
    char modeName[Modes][10] = {"Temp", "Test", "SFX", "Snake", "Tetris"};
    int nameWidth = strlen(modeName[selectedMode]) * LSize + LSize;

    // Transition effect
    gfxColorfill(1000);

    // State loop
    while (!exitingState) {
        gamepadUpdate();
        matrix.fillScreen(COLOR_BLACK);

        // Change selected state with RIGHT or LEFT key
        if (keyDown(RIGHT) && selectedMode < Modes - 1) {
            selectedMode++;
            nameWidth = strlen(modeName[selectedMode]) * LSize + LSize;
            textPosition = 0;
        } else if (keyDown(LEFT) && selectedMode > 0) {
            selectedMode--;
            nameWidth = strlen(modeName[selectedMode]) * LSize + LSize;
            textPosition = 0;
        }

        // Draw selection indicator
        matrix.drawLine(0, 0, Modes - 1, 0, COLOR_AQUA);
        matrix.drawPixel(selectedMode, 0, COLOR_WHITE);

        // Start selected state with B or A key
        if (keyDown(B) || keyDown(A)) {
            switch (selectedMode) {
                case 0: nextState = &stateTemp;         break;
                case 1: nextState = &stateInputCheck;   break;
                case 2: nextState = &stateLightshow;    break;
                case 3: nextState = &stateSnake;        break;
                case 4: nextState = &stateTetris;       break;
            }
            exitingState = true;
        }

        // Reset out of bounds scrolling text
        if (--textPosition < 0 - nameWidth) {
            textPosition = matrix.width();
        }

        // Draw name of selected mode
        matrix.setCursor(textPosition, 2);
        matrix.print(modeName[selectedMode]);

        //Draw pixels
        matrix.show();
        delay(REFRESH_RATE);
    }
    return;
}
