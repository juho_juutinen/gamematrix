/**
 * Copyright (c) 2015 Juho Juutinen.
 * 
 * This file is part of GameMatrix.
 * 
 * GameMatrix is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later 
 * version.
 * 
 * GameMatrix is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with GameMatrix. If not, see <http://www.gnu.org/licenses/>.
 *
 * @file:    stateTetris.ino
 * @author:  Juho Juutinen <juho.juutinen@iki.fi>
 * @created: 05/08/2015
 */

/*==== CONSTANT DEFINITIONS =================================================*/

const int GridWidth = 10;
const int GridHeight = 20;
const int TotalBlocks = 7;
const int DefaultDropDelay = 40;
const int MoveCooldown = 5;
const int SequenceLength = TotalBlocks * 4;

const uint16_t EmptyBlock = 0x0000;
const uint16_t MarkedBlock = 0x0001;

// Tetrimino struct
typedef struct Block {
    uint16_t element[4];
    uint16_t color;
};

// Tetrimino shape and color definitions
const struct Block Blocks[TotalBlocks] = {
{{0x0F00, 0x2222, 0x00F0, 0x4444}, 0x07FF},  // I-block
{{0x44C0, 0x8E00, 0x6440, 0x0E20}, 0x001F},  // J-block
{{0x4460, 0x0E80, 0xC440, 0x2E00}, 0xFD20},  // L-block
{{0xCC00, 0xCC00, 0xCC00, 0xCC00}, 0xFFE0},  // O-block
{{0x06C0, 0x8C40, 0x6C00, 0x4620}, 0x07E0},  // S-block
{{0x0E40, 0x4C40, 0x4E00, 0x4640}, 0xF81F},  // T-block
{{0x0C60, 0x4C80, 0xC600, 0x2640}, 0xF800}}; // Z-block

/*==== VARIABLE DEFINITIONS =================================================*/

int posX;
int posY;
int dropDelay;
int dropCount;
int currentDir;
int sequenceIndex;
int cooldown;
bool tetrisPaused;
bool tetrisGameOver;
Block currentBlock;
uint16_t rngSequence[SequenceLength];
uint16_t tetrisGrid[GridWidth][GridHeight];

/*==== STATE: TETRIS GAME ===================================================*/

void stateTetris() {

    // Transition effect
    gfxRainbow(1000);

    // Setup
    matrix.setRotation(MATRIX_VERTICAL);
    tetrisReset();

    // State loop
    while (!exitingState) {
        updateControls();
        
        // Pause the game with START key
        if (keyDown(START) && !tetrisGameOver) { tetrisPaused = !tetrisPaused; }
        
        // Reset the game with SELECT key
        if (keyDown(SELECT)) { tetrisReset(); }

        // Advance the game by one step
        if (!tetrisPaused && !tetrisGameOver) {

            // Rotate controlled block with A key
            if (keyDown(A)) { tetrisRotate(); }
            
            // Move controlled block with KEFT and RIGHT keys
            limit(cooldown, -1, 0, MoveCooldown);
            if (key(LEFT) && cooldown == 0) {
                if (tetrisFit(currentBlock, posX - 1, posY, currentDir)) {
                    posX--;
                    cooldown = MoveCooldown;
                }
            }

            if (key(RIGHT) && cooldown == 0) {
                if (tetrisFit(currentBlock, posX + 1, posY, currentDir)) {
                    posX++;
                    cooldown = MoveCooldown;
                }
            }

            // Drop controlled block with DOWN key
            if (key(DOWN)) {
                tetrisDrop();
                dropCount = dropDelay;
            }

           // Periodically drop controlled block by one step
           dropCount--;
           if (dropCount <= 0 && !tetrisGameOver) {
                tetrisDrop();
                dropCount = dropDelay;
           }
        }

        // Draw all tetris blocks
        tetrisRender();
        tetrisRenderBlock();

        // Draw a white line indicating paused state
        if (tetrisPaused) {
            int h = GridHeight / 2;
            int w = GridWidth - 1;
            matrix.drawLine(0, h, w, h, COLOR_WHITE);
        }
        
        // Draw pixels
        matrix.show();
        delay(FPS_60);
    }

    // Revert display orientation back to horizontal
    matrix.setRotation(MATRIX_HORIZONTAL);
    return;
}

/*==== STATE: TETRIS FUNCTIONS ==============================================*/

// Rotate controlled block clockwise
void tetrisRotate() {
    int newdir = currentDir + 1;
    if (newdir > 3) { newdir = 0; }
    if (tetrisFit(currentBlock, posX, posY, newdir)) {
        currentDir = newdir;
    }
}

// Lock controlled block to the grid
void tetrisLock() {
    int row = 0;
    int col = 0;

    for (uint16_t b = 0x8000; b > 0; b = b >> 1) {
        if (currentBlock.element[currentDir] & b) {
            tetrisGrid[posX + col][posY + row] = currentBlock.color;
        }
        if (++col == 4) {
            col = 0;
            ++row;
        }
    }
}

// Drop current block down by one row (if possible)
void tetrisDrop() {
    if (tetrisFit(currentBlock, posX, posY + 1, currentDir)) {
        posY++;
    } else {
        tetrisLock();
        if (posY <= 0) {
            tetrisLose();
            return;
        }
        tetrisClearRows();

        // Set current piece to next one in the sequence
        sequenceIndex++;
        if (sequenceIndex >= SequenceLength) {
            sequenceIndex = 0;
        }
        currentBlock = Blocks[rngSequence[sequenceIndex]];
        
        // Reset piece position
        currentDir = 0;
        posX = 3;
        posY = 0;
    }
}

// Find all full rows and remove them
void tetrisClearRows() {

    // Go through all rows
    for (int y = 0; y < GridHeight; y++) {
        bool fullRow = true;

        // Check if row is full
        for (int x = 0; x < GridWidth; x++) {
            if (tetrisGrid[x][y] == EmptyBlock) {
                fullRow = false;
                x = GridWidth;
            }
        }

        // Mark row for removal
        if (fullRow) { tetrisGrid[0][y] = MarkedBlock; }
    }

    // Remove all marked rows with a funky animation
    for (int x = 1; x < GridWidth; x++) {
        for (int y = 0; y < GridHeight; y++) {
            if (tetrisGrid[0][y] == MarkedBlock) {
                tetrisGrid[x][y] = EmptyBlock;
            }
        }
        tetrisRender();
        matrix.show();
        delay(FPS_30);
    }

    // Clear gaps left by removed rows
    for (int r = GridHeight - 1; r > 0; r--) {
        if (tetrisGrid[0][r] == MarkedBlock) {

            // Move all rows above it down
            for (int y = r; y > 0; y--) {
                for (int x = 0; x < GridWidth; x++) {
                    tetrisGrid[x][y] = tetrisGrid[x][y - 1];
                }
            }

            // Add a new empty row at the top of the grid
            for (int x = 0; x < GridWidth; x++) { tetrisGrid[x][0] = EmptyBlock; }

            // Start from the beginning again
            r = GridHeight;
        }
    }
}

// Generate a sequence of random pieces (each block type occurs four times)
void tetrisRandomizeSequence() {

    // Reset sequence
    for (int t = 0; t < SequenceLength; t++) { rngSequence[t] = -1; }

    // Populate sequence with random blocks
    for (int t = 0; t < TotalBlocks; t++) {
        for (int c = 0; c < 4; c++) {
            int i = 0;
            do {
                i = random(SequenceLength);
            } while (rngSequence[i] != -1);
            rngSequence[i] = t;
        }
    }
}

// Check if block [bt] fits in position [x],[y] with orientation [dir]
bool tetrisFit(struct Block bt, int x, int y, int dir) {
    int nx = 0, ny = 0, row = 0, col = 0;

    for (uint16_t b = 0x8000; b > 0; b = b >> 1) {
        if (bt.element[dir] & b) {
            nx = x + col;
            ny = y + row;
            if ((nx < 0) 
                 || (x >= GridWidth)
                 || (ny >= GridHeight)
                 || tetrisGrid[nx][ny]) {
                return false;
            }
        }
        if (++col == 4) {
          col = 0;
          ++row;
        }
    }

    return true;
}

// Handle losing the game
void tetrisLose() {
    tetrisGameOver = true;
    for (int x = 0; x < GridWidth; x++) {
        for (int y = 0; y < GridHeight; y++) {
            tetrisGrid[x][y] = color(50 + random(200), 0, 0);
        }
    }
}

// Completely reset game state
void tetrisReset() {
    for (int x = 0; x < GridWidth; x++) {
        for (int y = 0; y < GridHeight; y++) {
            tetrisGrid[x][y] = EmptyBlock;
        }
    }
    posX = 3;
    posY = 0;
    dropDelay = DefaultDropDelay;
    dropCount = dropDelay;
    tetrisGameOver = false;
    tetrisPaused = false;
    currentDir = 0;
    sequenceIndex = 0;
    tetrisRandomizeSequence();
    currentBlock = Blocks[rngSequence[sequenceIndex]];
}

// Render current game view
void tetrisRender() {
    matrix.fillScreen(COLOR_BLACK);
    for (int x = 0; x < GridWidth; x++) {
        for (int y = 0; y < GridHeight; y++) {
            matrix.drawPixel(x, y, tetrisGrid[x][y]);
        }
    }
}

// Render currently controlled tetris block
void tetrisRenderBlock() {
    int row = 0, col = 0;
    for (uint16_t b = 0x8000; b > 0; b = b >> 1) {
        if (currentBlock.element[currentDir] & b) {
            matrix.drawPixel(posX + col, posY + row, currentBlock.color);
        }
        if (++col == 4) {
            col = 0;
            ++row;
        }
    }
}
