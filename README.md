```
#!
===============================================================================
     ________                         _____          __         .__        
    /  _____/_____    _____   ____   /     \ _____ _/  |________|__|__  ___
   /   \  ___\__  \  /     \_/ __ \ /  \ /  \\__  \\   __\_  __ \  \  \/  /
   \    \_\  \/ __ \|  Y Y  \  ___//    Y    \/ __ \|  |  |  | \/  |>    < 
    \______  (____  /__|_|  /\___  >____|__  (____  /__|  |__|  |__/__/\_ \
           \/     \/      \/     \/        \/     \/                     \/
                                                                       [README]
===============================================================================
Contents
===============================================================================
 [1]  Introduction
 [2]  Requirements
 [3]  Licence Information

===============================================================================
[1]  Introduction
===============================================================================

GameMatrix is a multi-purpose program for arduino microcontrollers that are
connected to a LED strip and a NES controller. It contains games, visual
effects, wireless outdoor sauna temperature monitor and more.

See it in action: (http://www.youtube.com/watch?v=3gcXRkHGVw4)

===============================================================================
[2]  Requirements
===============================================================================

Arduino IDE is necessary to compile this project. (http://www.arduino.cc/)

The following libraries are also required:
 - JeeLib (https://github.com/jcw/jeelib)
 - AdaFruit GFX (https://github.com/adafruit/Adafruit-GFX-Library/)
 - AdaFruit NeoMatrix (https://github.com/adafruit/Adafruit_NeoMatrix/)
 - AdaFruit NeoPixel (http://www.adafruit.com/category/168)

===============================================================================
[3]  Licence Information
===============================================================================

Copyright (c) 2015 Juho Juutinen.

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program.  If not, see <http://www.gnu.org/licenses/>.

```