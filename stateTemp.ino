/**
 * Copyright (c) 2015 Juho Juutinen.
 * 
 * This file is part of GameMatrix.
 * 
 * GameMatrix is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later 
 * version.
 * 
 * GameMatrix is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with GameMatrix. If not, see <http://www.gnu.org/licenses/>.
 *
 * @file:    stateTemp.ino
 * @author:  Juho Juutinen <juho.juutinen@iki.fi>
 * @created: 04/21/2015
 */
 
/*==== CONSTANT/MACRO DEFINITIONS ===========================================*/

#define TEMPERATURE_TEST 0      // 1 = use fake temperature readings
#define TEST_STEP 4             // Rate of temp change when testing
#define NODE_ID 14              // Node ID of Rx (range 0-30)
#define NETWORK 210             // Network group (can be in the range 1-250)
#define RF69_COMPAT 0           // Use the RF69 driver i.s.o. RF12
#define RF_FREQ RF12_868MHZ     // Frequency of RF12B
#define NO_PAYLOAD -1000        // Indicates no sensor data is received yet

// Color gradient from blue to yellow to red
const uint16_t TemperatureRGB[10] = { 0x057C, 0x3E16, 0x7EAF, 0xBF49, 0xFFE2,
                                      0xFE42, 0xFCA1, 0xFB21, 0xF980, 0xF800};

/*==== STATE: TEMP MONITOR ==================================================*/

void stateTemp() {

    // Local variables
    int testTimer = 0;
    int currentTemperature = NO_PAYLOAD;
    typedef struct { int data; } Payload;
    Payload payload;

    // Init wireless receiver
    if (!TEMPERATURE_TEST) {
        rf12_initialize(NODE_ID, RF_FREQ, NETWORK);
    } else { currentTemperature = 0; }

    // Transition effect
    gfxVLinefill(COLOR_RED, 2000);

    // State loop
    while (!exitingState) {
        updateControls();
        matrix.fillScreen(COLOR_BLACK);
        matrix.setCursor(1, 2);

        // Check data from wireless receiver
        if (!TEMPERATURE_TEST) {
            if (rf12_recvDone()) {
                if (rf12_crc == 0 && (rf12_hdr & RF12_HDR_CTL) == 0) {
                    // Extract nodeID from payload
                    int nid = (rf12_hdr & 0x1F);

                    // Check if data is coming from node with the correct ID
                    if (nid == NODE_ID) {
                        payload = *(Payload*) rf12_data;
                        currentTemperature = payload.data;
                    }
                }
            }
        } else {
            if (++testTimer >= TEST_STEP) {
                testTimer = 0;
                currentTemperature++;
                wrap(currentTemperature, 0, 100);
            }
        }

        // Calculate color corresponding the temperature
        int tStep = round(currentTemperature / 70 * 9);
        limit(tStep, 0, 9);
        uint16_t tColor = TemperatureRGB[tStep];

        // Draw temperature bar on top of the screen
        matrix.drawLine(0, 0, round(currentTemperature / 4), 0, tColor);

        // Draw current temperature to the screen
        matrix.setTextColor(tColor);
        if (currentTemperature != NO_PAYLOAD) {
            matrix.print(currentTemperature);
            matrix.print(F("C"));
        } else {
            matrix.print(F("???"));
        }

        // Update pixels
        matrix.show();
        delay(FPS_60);
    }

    // Clean up
    matrix.setTextColor(TEXT_COLOR);
    return;
}